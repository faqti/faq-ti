/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.uniateneu.faqti.entity;

/**
 *
 * @author adler
 */
import jakarta.persistence.*;
import java.util.*;

@Entity
@Table(name="user")
public class Forum {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )         
    @Column(name="forum_id")
    private Integer forumId;
    
    @Column(name="user_id")
    private Integer userId;
    
          @Column(name="forum_create")
    private String forumCreate;
    
     @Column(name="forum_create_question")
    private String forumCreateQuestion;
     
      @Column(name="forum_create_response")
    private String forumCreateResponse;
      
            @Column(name="date_delete_forum")
    private Date dateDeleteforum;
            
                  @Column(name="date_delete_question")
    private Date dateDeleteQuestion;
                  
                  @Column(name="date_delete_response")
    private Date dateDeleteResponse;  
                  
                  @Column(name="date_update_forum")
    private Date dateUpdateForum;  
      
                  @Column(name="date_update_question")
    private Date dateUpdateQuestion;  
                  
                  @Column(name="date_update_response")
    private Date dateUpdateResponse; 
                                             
                  public Integer getForumId(){
        return forumId;
    }
    public void setForumId(Integer forumId){
        this.forumId = forumId;
         }
     public Integer getUserId(){
        return userId;
    }
    public void setUserId(Integer userId){
        this.userId = userId;
    }
     public String getForumCreate(){
        return forumCreate;
    }
    public void setForumCreate(String forumCreate){
        this.forumCreate = forumCreate;
    }
     public String getForumCreateQuestion(){
        return forumCreateQuestion;
    }
    public void setForumCreateQuestion(String forumCreateQuestion){
        this.forumCreateQuestion = forumCreateQuestion;
    }
      public String getForumCreateResponse(){
        return forumCreateResponse;
    }
    public void setForumCreateResponse(String forumCreateResponse){
        this.forumCreateResponse = forumCreateResponse;
    }
      public Date getDateDeleteResponse(){
        return dateDeleteResponse;
    }
    public void setDateDeleteResponse(Date dateDeleteResponse){
        this.dateDeleteResponse = dateDeleteResponse;
    }
     public Date getDateDeleteforum(){
        return dateDeleteforum;
    }
    public void setDateDeleteForum(Date dateDeleteForum){
    }
     public Date getDateDeleteQuestion(){
        return dateDeleteQuestion;
    }
    public void setDateDeleteQuestion(Date dateDeleteQuestion){
        this.dateDeleteQuestion = dateDeleteQuestion;
    }
     public Date getDateUpdateResponse(){
        return dateUpdateResponse;
    }
    public void setDateUpdateResponse(Date dateUpdateResponse){
        this.dateUpdateResponse = dateUpdateResponse;
    }
     public Date getDateUpdateforum(){
        return dateUpdateForum;
    }
    public void setDateUpdateForum(Date dateUpdateForum){
        this.dateUpdateForum = dateUpdateForum;
    }
     public Date getDateupdateQuestion(){
        return dateUpdateQuestion;
    }
    public void setDateupdateQuestion(Date dateUpdateQuestion){
        this.dateUpdateQuestion = dateUpdateQuestion;
    }
}
