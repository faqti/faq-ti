package br.edu.uniateneu.faqti.entity;

import jakarta.persistence.*;
import java.util.*;

@Entity
@Table(name="user")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)         
    @Column(name="user_id")
    private Integer userId;
    
    @Column(name="user_name")
    private String userName;
    
    @Column(name="user_email")
    private String userEmail;
    
    @Column(name="user_password")
    private String userPassword;
    
    @Column(name="date_create_user")
    private Date dateCreateUser;
    
    @Column(name="date_delete_user")
    private Date dateDeleteUser;

    public Integer getUserId(){
        return userId;
    }
    public void setUserId(Integer userId){
        this.userId = userId;
    }
    public String getUserName(){
        return userName;
    }
    public void setUserName(String userName){
        this.userName = userName;
    }
    public String getUserEmail(){
        return userEmail;
    }
    public void setUserEmail(String userEmail){
        this.userEmail = userEmail;
    }
    public String getUserPassword(){
        return userPassword;
    }
    public void setUserPassword(String userPassword){
        this.userPassword = userPassword;
    }  
    public Date getDateCreateUser(){
        return dateCreateUser;
    }
    public void setDateCreateUser(Date dateCreateUser){
        this.dateCreateUser = dateCreateUser;
    }
    public Date getDateDeleteUser(){
        return dateDeleteUser;
    }
    public void setDateDeleteUser(Date dateDeleteUser){
        this.dateDeleteUser = dateDeleteUser;
    }
     
   
}
