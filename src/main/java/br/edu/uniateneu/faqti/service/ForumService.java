/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.uniateneu.faqti.service;



/**
 *
 * @author adler
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.edu.uniateneu.faqti.repository.ForumRepository;
import br.edu.uniateneu.faqti.entity.Forum;

import java.util.List;
@Service

public class ForumService {
@Autowired
    ForumRepository forumRepository;

    public List<Forum> getAll(){
        return forumRepository.findAll();
    }

    public Forum getById(Integer id) {
        return forumRepository.findById(id).orElse(null) ;
    }

    public Forum saveForum(Forum forum) {
        return forumRepository.save(forum);
    }

    public Forum updateForum(Integer id, Forum forum) {
       Forum currentForum = forumRepository.findById(id).orElse(null);
        if(currentForum != null) {
            currentForum.setForumId(forum.getForumId());
           
            return forumRepository.save(currentForum);
        }else {
            return null;
        }
    }

    public Boolean deleteForum(Integer id) {
        Forum forum = forumRepository.findById(id).orElse(null);
        if(forum != null) {
           forumRepository.delete(forum);
            return true;
        }else {
            return false;
        }
    }
   
    
}
