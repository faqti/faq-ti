/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.uniateneu.faqti.service;

/**
 *
 * @author adler
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.edu.uniateneu.faqti.repository.UserRepository;
import br.edu.uniateneu.faqti.entity.Users;

@Service       
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<Users> getAll(){
        return userRepository.findAll();
    }

    public Users getById(Integer id) {
        return userRepository.findById(id).orElse(null) ;
    }

    public Users saveUser(Users user) {
        return userRepository.save(user);
    }

    public Users updateUser(Integer id, Users user) {
        Users currentUser = userRepository.findById(id).orElse(null);
        if(currentUser != null) {
            currentUser.setUserName(user.getUserName());
            currentUser.setUserEmail(user.getUserEmail());
            return userRepository.save(currentUser);
        }else {
            return null;
        }
    }

    public Boolean deleteUser(Integer id) {
        Users user = userRepository.findById(id).orElse(null);
        if(user != null) {
            userRepository.delete(user);
            return true;
        }else {
            return false;
        }
    }

}
