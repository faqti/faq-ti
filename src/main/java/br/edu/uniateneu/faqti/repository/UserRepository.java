package br.edu.uniateneu.faqti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import br.edu.uniateneu.faqti.entity.Users;
public interface UserRepository extends JpaRepository<Users, Integer> {
    
}
